window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
      // Here, add the 'd-none' class to the loading icon
      const hideSpinner = document.getElementById("loading-conference-spinner");
      hideSpinner.classList.add("d-none");
      // Here, remove the 'd-none' class from the select tag
      selectTag.classList.remove("d-none");
    }

    const getFormId = document.getElementById('create-attendee-form');
    getFormId.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(getFormId);
      const json = JSON.stringify(Object.fromEntries(formData));
      const attendeeURL = 'http://localhost:8001/api/attendees/';
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(attendeeURL, fetchConfig);
      if (response.ok) {
        getFormId.reset();
        const newAttendee = await response.json();
        getFormId.classList.add('d-none');
        const unhideSuccessMessage = document.getElementById('success-message');
        unhideSuccessMessage.classList.remove('d-none');
      }
    });
  });
